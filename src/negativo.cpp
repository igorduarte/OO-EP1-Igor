#include "negativo.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Negativo::Negativo(){}

void Negativo::aplicarNegativo(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, char *diretorio){
	ofstream newImgFile(diretorio);

	newImgFile << numeroMagico << endl;
	newImgFile << comentario << endl;
	newImgFile << largura << " " << altura << endl;
	newImgFile << nivelMaximoCinza << endl;

	for(int i=0; i<largura*altura; i++){
		char *pixelsCopia;
		pixelsCopia=new char[largura*altura];
		pixelsCopia=pixels;
		pixelsCopia[i]=nivelMaximoCinza-pixelsCopia[i];
		newImgFile.put(pixelsCopia[i]);
	}
}
