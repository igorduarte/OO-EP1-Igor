#ifndef IMAGEM_H
#define IMAGEM_H

#include <string>
#include <iostream>

using namespace std;

class Imagem{
	private:
		string numeroMagico, comentario;
		int altura, largura, nivelMaximoCinza;
		char *pixels, *diretorio;
	public:
		Imagem();
		Imagem(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels);

		void setNumeroMagico(string numeroMagico);
	        string getNumeroMagico();

		void setComentario(string comentario);
		string getComentario();

		void setAltura(int altura);
		int getAltura();
	    
		void setLargura(int largura);
		int getLargura();
		
		void setNivelMaximoCinza(int nivelMaximoCinza);
		int getNivelMaximoCinza();  

		void setPixels(char *pixels);
		char *getPixels();
		
		void copiarImagem(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, char *diretorio);
};

#endif
