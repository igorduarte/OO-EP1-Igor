#ifndef FILTRO_H
#define FILTRO_H

class Filtro{
	private:
		int div, size, *filter;
		char *diretorio;
		
	public:
		Filtro();
		Filtro(int div, int size, int *filter, char *diretorio);

		void setDiv(int div);
		int getDiv();

		void setSize(int size);
		int getSize();
	
		void setFilter(int *filter);
		int *getFilter();

		void setDiretorio(char *diretorio);
		char *getDiretorio();
};

#endif
