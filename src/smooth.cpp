#include "smooth.hpp"

#include <string>
#include <fstream>

using namespace std;

Smooth::Smooth(){}

void Smooth::aplicarSmooth(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, int *filter, int size, int div, char *diretorio){

	char *pixelsCopia;
	pixelsCopia=new char[largura*altura];

	for(int i=0; i<largura*altura; i++){
		pixelsCopia[i]=pixels[i];
	}

	for(int i=0; i<largura; i++){
		for(int j=0; j<altura; j++){
			int value=0;
			for(int x=-1; x<=1; x++){
				for(int y=-1; y<=1; y++){
					value+=filter[(x+1)+size*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*largura];
				}
			}
			value/=div;
			value=value < 0 ? 0 : value;
			value=value > 255 ? 255 : value;
			pixelsCopia[i+j*largura]=value;
		}
	}
	
	ofstream newImgFile(diretorio);
	newImgFile << numeroMagico << endl;
	newImgFile << comentario << endl;
	newImgFile << largura << " " << altura << endl;
	newImgFile << nivelMaximoCinza << endl;

	for(int i=0; i<largura*altura; i++){
		newImgFile.put(pixelsCopia[i]);
	}

}
